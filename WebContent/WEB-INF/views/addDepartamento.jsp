<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Departamentos</title>
</head>
<body>
	<c:import url="menu.jsp" />
	
	<c:url var="save" value="/departamento/save" />
	<form:form modelAttribute="departamento" action="${save}" method="post">
		<form:hidden path="id"/>
		<fieldset style="width: 500px; margin: 0 auto;">
			<legend>Departamento</legend>
			<div>
				<form:label path="departamento">Departamento</form:label><br>
				<form:input path="departamento" type="text" required="true" />
			</div>
			<br>
			<div>
				<input type="submit" value="Salvar">
				<input type="reset" value="Limpar">
			</div>
		</fieldset>
	</form:form>
	<fieldset style="width: 500px; margin: 0 auto;">
		<legend>Departamentos</legend>
		<table style="width: 490px;">
			<tr>
				<th>C�digo</th>
				<th>Descri��o</th>
				<th>A��o</th>
			</tr>
			<c:forEach var="d" items="${departamentos}" varStatus="i">
				<tr bgcolor="${i.count % 2 != 0 ? '#F1F1F1' : 'white'}">
					<td>${d.id}</td>
					<td>${d.departamento}</td>
					<td>
						<c:url var="update" value="/departamento/update/${d.id}" />
						<a href="${update}" title="Ver/Editar">&#9445;</a>
						<c:url var="delete" value="/departamento/delete/${d.id}" />
						<a href="${delete}" title="Delete">&#9447;</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<div align="center">
			<c:if test="${current != 1}">
				<c:url var="prev" value="/departamento/page/${current - 1}" />
				<a href="${prev}" title="Prev">&laquo;</a>
			</c:if>
			${current}
			<c:if test="${current >= 1 && (current + 1) <= total}">
				<c:url var="next" value="/departamento/page/${current + 1}" />
				<a href="${next}" title="Next">&raquo;</a>
			</c:if>
		</div>
	</fieldset>
</body>
</html>