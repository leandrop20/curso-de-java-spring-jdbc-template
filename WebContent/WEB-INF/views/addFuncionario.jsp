<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Funcion�rios</title>
	<c:url var="cssUrl" value="/css/style.css"/>
	<link rel="stylesheet" type="text/css" href="${cssUrl}">
	<c:url var="jsUrl" value="/js/functions.js"/>
	<script type="text/javascript" src="${jsUrl}"></script>
</head>
<body>
	<c:import url="menu.jsp" />
	
	<fieldset class="master">
		<c:url var="save" value="/funcionario/save" />
		<form:form modelAttribute="funcionario" action="${save}" method="post">
			<form:errors cssClass="errorblock" path="*" element="div" />
			<form:hidden path="id"/>
			<fieldset class="grupo">
				<legend>Funcion�rio</legend>
				<div class="campo">
					<form:label path="nome" >Nome</form:label><br>
					<form:input path="nome" type="text" size="30" id="nome" />
				</div>
				<div class="campo">
					<form:label path="salario" >Sal�rio</form:label><br>
					<form:input path="salario" type="text" size="15" />
				</div>
				<div class="campo">
					<form:label path="dataEntrada" >Data de Entrada</form:label><br>
					<form:input path="dataEntrada" type="date" />
				</div>
				<div class="campo">
					<form:label path="dataSaida" >Data de Sa�da</form:label><br>
					<form:input path="dataSaida" type="date" />
				</div>
				<input type="button" value="Localizar" onclick="localizarPorNome()">
			</fieldset>
			<fieldset class="grupo">
				<legend>Cargo</legend>
				<div class="campo">
					<form:label path="cargo">Cargo</form:label><br>
					<form:select id="cargo" path="cargo">
						<form:option value="" label="--- Select ---" />
						<form:options items="${cargos}" itemValue="id" itemLabel="cargo" />
					</form:select>
				</div>
				<div class="campo">
					<input type="button" onclick="localizarPorCargo()" value="Localizar" />
				</div>
			</fieldset>
			<br>
			<fieldset class="grupo">
				<legend>Endere�o</legend>
				<form:hidden path="endereco.id"/>
				<div class="campo">
					<form:label path="endereco.logradouro" >Logradouro</form:label><br>
					<form:input path="endereco.logradouro" type="text" size="30" />
					<form:errors cssClass="error" path="endereco.logradouro" element="div" />
				</div>
				<div class="campo">
					<form:label path="endereco.numero" >N�mero</form:label><br>
					<form:input path="endereco.numero" type="text" size="30" />
					<form:errors cssClass="error" path="endereco.numero" element="div" />
				</div>
				<div class="campo">
					<form:label path="endereco.complemento" >Complemento</form:label><br>
					<form:input path="endereco.complemento" type="text" size="30" />
					<div class="error">&nbsp;</div>
				</div>
				<div class="campo">
					<form:label path="endereco.bairro" >Bairro</form:label><br>
					<form:input path="endereco.bairro" type="text" size="30" />
					<form:errors cssClass="error" path="endereco.bairro" element="div" />
				</div>
				<div class="campo">
					<form:label path="endereco.cidade" >Cidade</form:label><br>
					<form:input path="endereco.cidade" type="text" size="30" />
					<form:errors cssClass="error" path="endereco.cidade" element="div" />
				</div>
				<div class="campo">
					<form:label path="endereco.estado" >Estado</form:label><br>
					<form:input path="endereco.estado" type="text" size="30" />
					<form:errors cssClass="error" path="endereco.estado" element="div" />
				</div>
			</fieldset>
			<br>
			<div>
				<input type="submit" value="Salvar" />
				<input type="reset" value="Limpar" />
			</div>
		</form:form>
	</fieldset>
	<fieldset class="master">
		<legend>Funcion�rios</legend>
		<table style="width: 700px;">
			<tr>
				<th>C�digo</th>
				<th>Nome</th>
				<th>Sal�rio</th>
				<th>Data de Entrada</th>
				<th>Cargo</th>
				<th>A��o</th>
			</tr>
			<c:forEach var="f" items="${funcionarios}" varStatus="i">
				<tr bgcolor="${i.count % 2 != 0 ? '#F1F1F1' : 'white'}">
					<td>${f.id}</td>
					<td>${f.nome}</td>
					<td>
						<fmt:formatNumber value="${f.salario}" 
							currencySymbol="R$" maxFractionDigits="2"
							type="currency" />
					</td>
					<td>
						<fmt:parseDate var="dtEntrada"
							value="${f.dataEntrada}" 
							pattern="yyyy-MM-dd" />
						<fmt:formatDate value="${dtEntrada}"
							dateStyle="short" />
					</td>
					<td>${f.cargo.cargo}</td>
					<td>
						<c:url var="update" value="/funcionario/update/${f.id}" />
						<a href="${update}" title="Ver/Editar">&#9445;</a>
						<c:url var="delete" value="/funcionario/delete/${f.id}" />
						<a href="${delete}" title="Delete">&#9447;</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<div align="center">
			<c:if test="${current != 1}">
				<c:url var="prev" value="/funcionario/page/${current - 1}" />
				<a href="${prev}" title="Prev">&laquo;</a>
			</c:if>
			${current}
			<c:if test="${current >= 1 && (current + 1) <= total}">
				<c:url var="next" value="/funcionario/page/${current + 1}" />
				<a href="${next}" title="Next">&raquo;</a>
			</c:if>
		</div>
	</fieldset>
</body>
</html>