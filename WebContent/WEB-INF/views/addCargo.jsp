<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cargos</title>
</head>
<body>
	<c:import url="menu.jsp" />

	<c:url var="save" value="/cargo/save" />
	<form:form modelAttribute="cargo" action="${save}" method="post">
		<form:hidden path="id"/>
		<fieldset style="width: 500px; margin: 0 auto;">
			<legend>Cargo</legend>
			<div>
				<form:label path="cargo">Cargo</form:label><br>
				<form:input path="cargo" type="text" required="true" />
			</div>
			<br>
			<div>
				<form:label path="cargo">Departamento</form:label><br>
				<form:select path="departamento" required="true">
					<form:option value="" label="--- Select ---" />
					<form:options items="${departamentos}" itemLabel="departamento" itemValue="id" />
				</form:select>
			</div>
			<br>
			<div>
				<input type="submit" value="Salvar">
				<input type="reset" value="Limpar">
			</div>
		</fieldset>
	</form:form>
	<fieldset style="width: 500px; margin: 0 auto;">
		<legend>Cargos</legend>
		<table style="width: 500px;">
			<tr>
				<th>C�digo</th>
				<th>Descri��o</th>
				<th>Departamento</th>
				<th>A��o</th>
			</tr>
			<c:forEach var="c" items="${cargos}" varStatus="i">
				<tr bgcolor="${i.count % 2 != 0 ? '#F1F1F1' : 'white'}">
					<td>${c.id}</td>
					<td>${c.cargo}</td>
					<td>${c.departamento.departamento}</td>
					<td>
						<c:url var="update" value="/cargo/update/${c.id}" />
						<a href="${update}" title="Ver/Editar">&#9445;</a>
						<c:url var="delete" value="/cargo/delete/${c.id}" />
						<a href="${delete}" title="Delete">&#9447;</a>
					</td>
				</tr>
			</c:forEach>
		</table>
		<div align="center">
			<c:if test="${current != 1}">
				<c:url var="prev" value="/cargo/page/${current - 1}" />
				<a href='${prev}' title="Prev">&laquo;</a>				
			</c:if>
			${current}
			<c:if test="${current >= 1 && (current + 1) <= total}">
				<c:url var="next" value="/cargo/page/${current + 1}" />
				<a href="${next}" title="p�gina(s) de ${total}">&raquo;</a>
			</c:if>
		</div>
	</fieldset>
</body>
</html>