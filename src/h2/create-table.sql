CREATE TABLE departamentos (
	id INT NOT NULL IDENTITY,
	departamento VARCHAR(60)
);

CREATE TABLE cargos (
	id INT NOT NULL IDENTITY,
	cargo VARCHAR(80) NOT NULL,
	departamento_id INT NOT NULL,
	CONSTRAINT FK_DEPARTAMENTOS FOREIGN KEY (departamento_id) REFERENCES departamentos(id) ON DELETE CASCADE
);

CREATE TABLE enderecos(
	id INT NOT NULL IDENTITY,
	logradouro VARCHAR(80) NOT NULL,
	numero INT NOT NULL,
	complemento VARCHAR(12) NOT NULL,
	cidade VARCHAR(45) NOT NULL,
	bairro VARCHAR(45) NOT NULL,
	estado VARCHAR(45) NOT NULL
);

CREATE TABLE funcionarios (
	id INT NOT NULL IDENTITY,
	cargo_id INT NOT NULL,
	endereco_id INT NOT NULL,
	nome VARCHAR(80) NOT NULL,
	salario DOUBLE NOT NULL,
	data_entrada DATE NOT NULL,
	data_saida DATE,
	CONSTRAINT FK_CARGOS FOREIGN KEY (cargo_id) REFERENCES cargos(id),
	CONSTRAINT FK_ENDERECOS FOREIGN KEY (endereco_id) REFERENCES enderecos(id)
);