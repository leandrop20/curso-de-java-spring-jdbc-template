package br.com.devmedia.appfinal.service;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.devmedia.appfinal.dao.DepartamentoDao;
import br.com.devmedia.appfinal.entity.Departamento;

@Service
public class DepartamentoService {

	private static final Logger logger = LogManager.getLogger(DepartamentoService.class);
	
	@Autowired
	private DepartamentoDao dao;
	
	public Departamento save(Departamento departamento) {
		return dao.save(departamento);
	}
	
	public void update(Departamento departamento) {
		dao.update(departamento);
	}
	
	public void saveOrUpdate(Departamento departamento) {
		if (departamento.getId() == null) {
			logger.info("Salvando um departamento!");
			dao.save(departamento);
			logger.info("Departamento " + departamento.getId() + " salvo com sucesso!");
		} else {
			logger.info("Alterando um departamento!");
			dao.update(departamento);
		}
	}
	
	public void delete(Integer id) {
		dao.delete(id);
	}
	
	public Departamento findByID(Integer id) {
		return dao.findByID(id);
	}
	
	public List<Departamento> findAll() {
		return dao.findAll();
	}
	
	public int getTotalPages(int size) {
		String sql = "SELECT count(*) FROM departamentos";
		int count = dao.getJdbcTemplate()
				.queryForObject(sql, Integer.class);
		return (int) Math.ceil(new Double(count) / new Double(size));
	}
	
	public List<Departamento> findByPage(int page, int size) {
		return dao.findByPage((page - 1) * size, size);
	}
	
}