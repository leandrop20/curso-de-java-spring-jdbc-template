package br.com.devmedia.appfinal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.appfinal.dao.FuncionarioDao;
import br.com.devmedia.appfinal.entity.Endereco;
import br.com.devmedia.appfinal.entity.Funcionario;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class FuncionarioService {

	@Autowired
	private FuncionarioDao dao;
	@Autowired
	private EnderecoService enderecoService;
	
	public Funcionario saveOrUpdate(Funcionario funcionario) {
		Endereco endereco = enderecoService.saveOrUpdate(funcionario.getEndereco());
		funcionario.setEndereco(endereco);
		
		if (funcionario.getId() == null) {
			dao.save(funcionario);
		} else {
			dao.update(funcionario);
		}
		return funcionario;
	}
	
	public void delete(Integer id) {
		dao.delete(id);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public Funcionario findByID(Integer id) {
		return dao.findByID(id);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Funcionario> findAll() {
		return dao.findAll();
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Funcionario> findByCargo(Integer cargoID) {
		return dao.findByCargo(cargoID);
	}
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public List<Funcionario> findByNome(String nome) {
		return dao.findByNome(nome);
	}
	
	public int getTotalPages(int size) {
		String sql = "SELECT count(*) FROM funcionarios";
		int count = dao.getJdbcTemplate()
				.queryForObject(sql, Integer.class);
		return (int) Math.ceil(new Double(count) / new Double(size));
	}
	
	public List<Funcionario> findByPage(int page, int size) {
		return dao.findByPage((page - 1) * size, size);
	}
	
}
