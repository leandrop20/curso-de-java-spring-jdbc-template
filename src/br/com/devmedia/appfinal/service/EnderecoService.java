package br.com.devmedia.appfinal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.devmedia.appfinal.dao.EnderecoDao;
import br.com.devmedia.appfinal.entity.Endereco;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class EnderecoService {

	@Autowired
	private EnderecoDao dao;
	
	@Transactional(rollbackFor = Exception.class)
	public Endereco saveOrUpdate(Endereco endereco) {
		if (endereco.getId() == null) {
			dao.save(endereco);
		} else {
			dao.update(endereco);
		}
		return endereco;
	}
	
	public void delete(Integer id) {
		dao.delete(id);
	}
	
	public Endereco findByID(Integer id) {
		return dao.findByID(id);
	}
	
	public List<Endereco> findAll() {
		return dao.findAll();
	}
	
}
