package br.com.devmedia.appfinal.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.devmedia.appfinal.dao.CargoDao;
import br.com.devmedia.appfinal.entity.Cargo;

@Service
public class CargoService {
		
	@Autowired
	private CargoDao dao;
	
	public void saveOrUpdate(Cargo cargo) {
		if (cargo.getId() == null) {
			dao.save(cargo);
		} else {
			dao.update(cargo);
		}
	}
	
	public void delete(Integer id) {
		dao.delete(id);
	}
	
	public Cargo findByID(Integer id) {
		return dao.findByID(id);
	}
	
	public List<Cargo> findAll() {
		return dao.findAll();
	}
	
	public int getTotalPages(int size) {
		String sql = "SELECT count(*) FROM cargos";
		int count = dao.getJdbcTemplate().
				queryForObject(sql, Integer.class);
		return (int) Math.ceil(new Double(count) / new Double(size));
	}
	
	public List<Cargo> findByPage(int page, int size) {
		return dao.findByPage((page - 1) * size, size);
	}
	
}
