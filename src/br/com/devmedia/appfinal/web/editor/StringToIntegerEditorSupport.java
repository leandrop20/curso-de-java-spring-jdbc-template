package br.com.devmedia.appfinal.web.editor;

import java.beans.PropertyEditorSupport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringToIntegerEditorSupport extends PropertyEditorSupport {
	
	private static Logger logger = LogManager.getLogger(StringToIntegerEditorSupport.class);
	
	@Override
	public void setAsText(String text) {
		try {
			super.setValue(Integer.parseInt(text));
		} catch (NumberFormatException ex) {
			logger.fatal("O campo n�mero esperava um inteiro e receveu uma string!", ex);
		}
	}
	
}
