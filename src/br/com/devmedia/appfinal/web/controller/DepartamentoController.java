package br.com.devmedia.appfinal.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.devmedia.appfinal.entity.Departamento;
import br.com.devmedia.appfinal.service.DepartamentoService;

@Controller
@RequestMapping("departamento")
public class DepartamentoController {

	private static final int PAGE_DEFAULT = 1;
	private static final int PAGE_SIZE_DEFAULT = 3;
	
	@Autowired
	private DepartamentoService service;
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView findAll(
		@ModelAttribute("departamento") Departamento departamento,
		ModelMap model) {
		model.addAttribute(
				"departamentos",
				service.findByPage(PAGE_DEFAULT, PAGE_SIZE_DEFAULT));
		model.addAttribute("current", PAGE_DEFAULT);
		model.addAttribute("total", service.getTotalPages(PAGE_SIZE_DEFAULT));
		return new ModelAndView("addDepartamento", model);
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String save(@ModelAttribute("departamento") Departamento departamento) {
		service.saveOrUpdate(departamento);
		return "redirect:/departamento/add";
	}
	
	@RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
	public ModelAndView preUpdate(@PathVariable("id") Integer id, ModelMap model) {
		Departamento d = service.findByID(id);
		model.addAttribute("id", id);
		model.addAttribute("departamento", d.getDepartamento());
		model.addAttribute("current", PAGE_DEFAULT);
		model.addAttribute("total", service.getTotalPages(PAGE_SIZE_DEFAULT));
		return new ModelAndView("redirect:/departamento/add", model);
	}
	
	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
	public String delete(@PathVariable("id") Integer id) {
		service.delete(id);
		return "redirect:/departamento/add";
	}
	
	
	
	@RequestMapping(value = "/page/{page}")
	public ModelAndView pagination(
			@PathVariable("page") Integer page,
			@ModelAttribute("departamento") Departamento departamento) {
		ModelAndView model = new ModelAndView("addDepartamento");
		model.addObject(
				"departamentos",
				service.findByPage(page, PAGE_SIZE_DEFAULT));
		model.addObject("current", page);
		model.addObject("total", service.getTotalPages(PAGE_SIZE_DEFAULT));
		return model;
	}
	
}
