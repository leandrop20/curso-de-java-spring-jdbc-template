package br.com.devmedia.appfinal.web.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.devmedia.appfinal.entity.Cargo;
import br.com.devmedia.appfinal.entity.Funcionario;
import br.com.devmedia.appfinal.service.CargoService;
import br.com.devmedia.appfinal.service.EnderecoService;
import br.com.devmedia.appfinal.service.FuncionarioService;
import br.com.devmedia.appfinal.web.editor.CargoEditorSupport;
import br.com.devmedia.appfinal.web.editor.StringToDoubleEditorSupport;
import br.com.devmedia.appfinal.web.editor.StringToIntegerEditorSupport;
import br.com.devmedia.appfinal.web.validator.EnderecoValidator;
import br.com.devmedia.appfinal.web.validator.FuncionarioValidator;

@Controller
@RequestMapping("funcionario")
public class FuncionarioController {
	
	private static final int PAGE_DEFAULT = 1;
	private static final int PAGE_SIZE_DEFAULT = 3;
	
	private static final Logger logger = LogManager.getLogger(Funcionario.class);
	
	@Autowired
	private FuncionarioService funcionarioService;
	@Autowired
	private CargoService cargoService;
	@Autowired
	private EnderecoService enderecoService;
	
	@InitBinder
	protected void initBinder(ServletRequestDataBinder binder) {
		binder.registerCustomEditor(Cargo.class, new CargoEditorSupport(cargoService));
		binder.registerCustomEditor(Integer.class, "endereco.numero", new StringToIntegerEditorSupport());
		binder.registerCustomEditor(Double.class, "salario", new StringToDoubleEditorSupport());
		binder.addValidators(new FuncionarioValidator(new EnderecoValidator()));
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public ModelAndView findAll(@ModelAttribute("funcionario") Funcionario funcionario) {
		ModelAndView modelAndView = new ModelAndView("addFuncionario");
		modelAndView.addObject(
				"funcionarios",
				funcionarioService.findByPage(PAGE_DEFAULT, PAGE_SIZE_DEFAULT));
		modelAndView.addObject("cargos", cargoService.findAll());
		modelAndView.addObject("current", PAGE_DEFAULT);
		modelAndView.addObject("total", funcionarioService.getTotalPages(PAGE_SIZE_DEFAULT));
		return modelAndView;
	}
	
	@RequestMapping(value = "/save")
	public ModelAndView save(@ModelAttribute("funcionario") @Validated Funcionario funcionario,
			BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			logger.warn("Foram encontrados campos inválidos!");
			model.addAttribute(
					"funcionarios",
					funcionarioService.findByPage(PAGE_DEFAULT, PAGE_SIZE_DEFAULT));
			model.addAttribute("cargos", cargoService.findAll());
			return new ModelAndView("addFuncionario", model);
		}
		
		try {
			funcionarioService.saveOrUpdate(funcionario);
			logger.error("Operação realizada com sucesso para funcionário id["
					+ funcionario.getId() + "!]");
		} catch (Exception e) {
			logger.error("Erro ao inserir/alterar um funcionário!", e);
		}
		return new ModelAndView("redirect:/funcionario/add");
	}
	
	@RequestMapping(value = "/update/{id}")
	public ModelAndView preUpDate(@PathVariable("id") Integer id, ModelMap model) {
		model.addAttribute("funcionario", funcionarioService.findByID(id));
		model.addAttribute(
				"funcionarios",
				funcionarioService.findByPage(PAGE_DEFAULT, PAGE_SIZE_DEFAULT));
		model.addAttribute("cargos", cargoService.findAll());
		model.addAttribute("current", PAGE_DEFAULT);
		model.addAttribute("total", funcionarioService.getTotalPages(PAGE_SIZE_DEFAULT));
		return new ModelAndView("addFuncionario");
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String delete(@PathVariable("id") Integer id) {
		Funcionario f = funcionarioService.findByID(id);
		enderecoService.delete(f.getEndereco().getId());
		return "redirect:/funcionario/add";
	}
	
	@RequestMapping(value = "/find/cargo/{cargo_id}", method = RequestMethod.GET)
	public ModelAndView findByCargo(
			@PathVariable("cargo_id") Integer cargoID,
			@ModelAttribute("funcionario") Funcionario funcionario,
			ModelMap model) {
		model.addAttribute("funcionarios", funcionarioService.findByCargo(cargoID));
		model.addAttribute("cargos", cargoService.findAll());
		model.addAttribute("current", PAGE_DEFAULT);
		model.addAttribute("total", funcionarioService.getTotalPages(PAGE_SIZE_DEFAULT));
		return new ModelAndView("addFuncionario", model);
	}
	
	@RequestMapping(value = "/find/nome/{nome}")
	public ModelAndView findByNome(
			@PathVariable("nome") String nome,
			@ModelAttribute("funcionario") Funcionario funcionario,
			ModelMap model) {
		model.addAttribute("funcionarios", funcionarioService.findByNome(nome));
		model.addAttribute("cargos", cargoService.findAll());
		model.addAttribute("current", PAGE_DEFAULT);
		model.addAttribute("total", funcionarioService.getTotalPages(PAGE_SIZE_DEFAULT));
		return new ModelAndView("addFuncionario", model);
	}
	
	@RequestMapping(value = "/page/{page}")
	public ModelAndView pagination(
			@PathVariable("page") Integer page,
			@ModelAttribute("funcionario") Funcionario funcionario) {
		ModelAndView model = new ModelAndView("addFuncionario");
		model.addObject("cargos", cargoService.findAll());
		model.addObject(
				"funcionarios",
				funcionarioService.findByPage(page, PAGE_SIZE_DEFAULT));
		model.addObject("current", page);
		model.addObject("total", funcionarioService.getTotalPages(PAGE_SIZE_DEFAULT));
		return model;
	}
	
}
