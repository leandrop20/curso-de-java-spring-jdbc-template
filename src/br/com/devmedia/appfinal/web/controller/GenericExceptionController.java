package br.com.devmedia.appfinal.web.controller;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class GenericExceptionController {

	private static Logger logger = LogManager.getLogger(GenericExceptionController.class);
	private static final String DEFAULT_PAGE_ERROR = "error";
	
	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView notFoundException(HttpServletRequest req, Exception ex) {
		logger.error("Request: " + req.getRequestURI() + " lan�ou a ex: " + ex);
		
		ModelAndView model = new ModelAndView(DEFAULT_PAGE_ERROR);
		model.addObject("mensagem", "Ops! Est� p�gina n�o existe! :(");
		model.addObject("exception", ex);
		model.addObject("url", req.getRequestURL());
		return model;
	}
	
	@ExceptionHandler(DataIntegrityViolationException.class)
	public ModelAndView integrityKeyException(HttpServletRequest req, DataIntegrityViolationException ex) {
		logger.error("Request: " + req.getRequestURI() + " lan�ou a ex: " + ex);
		
		ModelAndView model = new ModelAndView(DEFAULT_PAGE_ERROR);
		model.addObject("mensagem", "Imposs�vel inserir, registro j� existe!");
		model.addObject("exception", ex);
		model.addObject("url", req.getRequestURL());
		return model;
	}
	
	@ExceptionHandler({SQLException.class, DataAccessException.class})
	public ModelAndView sqlException(HttpServletRequest req, Exception ex) {
		logger.error("Request: " + req.getRequestURI() + " lan�ou a ex: " + ex);
		
		ModelAndView model = new ModelAndView(DEFAULT_PAGE_ERROR);
		model.addObject("mensagem", "Ocorreu algum problema ao acessar o DB!");
		model.addObject("exception", ex);
		model.addObject("url", req.getRequestURL());
		return model;
	}
	
	@ExceptionHandler(Exception.class)
	public ModelAndView defaultException(HttpServletRequest req, Exception ex) {
		logger.error("Request: " + req.getRequestURI() + " lan�ou a ex: " + ex);
		
		ModelAndView model = new ModelAndView(DEFAULT_PAGE_ERROR);
		model.addObject("mensagem", "Ocorreu um problema! :(");
		model.addObject("exception", ex);
		model.addObject("url", req.getRequestURL());
		return model;
	}
	
}
