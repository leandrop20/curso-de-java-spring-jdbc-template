package br.com.devmedia.appfinal.web.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.devmedia.appfinal.entity.Endereco;

public class EnderecoValidator implements Validator {

	public boolean supports(Class<?> clazz) {
		return Endereco.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmpty(
			errors, "endereco.logradouro", "error.logradouro");
		
		ValidationUtils.rejectIfEmpty(
			errors, "endereco.bairro", "error.bairro");
		
		ValidationUtils.rejectIfEmpty(
			errors, "endereco.cidade", "error.cidade");
		
		ValidationUtils.rejectIfEmpty(
			errors, "endereco.estado", "error.estado");
		
		Endereco e = (Endereco) target;
		if (e.getNumero() != null) {
			if (e.getNumero() < 0) {
				errors.rejectValue("endereco.numero", "error.numero.negativo");
			}
		} else {
			errors.rejectValue("endereco.numero", "error.numero");
		}
	}
	
}
