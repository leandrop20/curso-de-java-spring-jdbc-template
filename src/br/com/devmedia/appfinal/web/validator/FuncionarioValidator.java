package br.com.devmedia.appfinal.web.validator;

import java.time.LocalDate;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import br.com.devmedia.appfinal.entity.Funcionario;

public class FuncionarioValidator implements Validator {

	private EnderecoValidator enderecoValidator;
	
	public FuncionarioValidator(EnderecoValidator enderecoValidator) {
		this.enderecoValidator = enderecoValidator;
	}

	public boolean supports(Class<?> clazz) {
		return Funcionario.class.equals(clazz);
	}

	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nome", "error.nome", "O campo Nome � obrigat�rio!");
		
		Funcionario f = (Funcionario) target;
		
		if (f.getSalario() != null) {
			if (f.getSalario() < 0) {
				errors.rejectValue("salario", "error.salario", "O sal�rio n�o deve ser negativo!");
			}
		} else {
			errors.rejectValue("salario", "error.salario", "O campo sal�rio � obrigat�rio!");
		}
		
		if (f.getDataEntrada() != null) {
			LocalDate atual = LocalDate.now();
			if (f.getDataEntrada().isAfter(atual)) {
				errors.rejectValue("dataEntrada", "error.dataEntrada", "A data de entrada deve ser anterior ou igual a data atual!");
			}
		} else {
			errors.rejectValue("dataEntrada", "error.dataEntrada", "O campo data de entrada � obrigat�rio!");
		}
		
		if (f.getDataSaida() != null) {
			if (f.getDataSaida().isBefore(f.getDataEntrada())) {
				errors.rejectValue("dataSaida", "error.dataSaida", "A data de saida deve ser posterior ou igual a data de entrada!");
			}
		}
		
		if (f.getCargo() == null) {
			errors.rejectValue("cargo", "error.cargo", "O campo cargo � obrigat�rio!");
		}
		
		ValidationUtils.invokeValidator(enderecoValidator, f.getEndereco(), errors);
	}
	
}
