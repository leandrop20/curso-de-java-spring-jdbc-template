package br.com.devmedia.appfinal.entity;

public class Cargo {

	private Integer id;
	private String cargo;
	private Departamento departamento;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCargo() {
		return cargo;
	}
	
	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
	
	public Departamento getDepartamento() {
		return departamento;
	}
	
	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	@Override
	public String toString() {
		return "Cargo [id=" + id + ", cargo=" + cargo + ", departamento=" + departamento + "]";
	}
	
}
