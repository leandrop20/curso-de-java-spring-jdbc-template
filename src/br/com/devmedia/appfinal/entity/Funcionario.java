package br.com.devmedia.appfinal.entity;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class Funcionario {

	private Integer id;
	private String nome;
	@DateTimeFormat(iso = ISO.DATE, pattern="yyyy-MM-dd")
	private LocalDate dataEntrada;
	@DateTimeFormat(iso = ISO.DATE, pattern="yyyy-MM-dd")
	private LocalDate dataSaida;
	//@NumberFormat(style = Style.CURRENCY, pattern="###,###.00")
	private Double salario;
	private Cargo cargo;
	private Endereco endereco;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public LocalDate getDataEntrada() {
		return dataEntrada;
	}
	
	public void setDataEntrada(LocalDate dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	
	public LocalDate getDataSaida() {
		return dataSaida;
	}
	
	public void setDataSaida(LocalDate dataSaida) {
		this.dataSaida = dataSaida;
	}
	
	public Double getSalario() {
		return salario;
	}
	
	public void setSalario(Double salario) {
		this.salario = salario;
	}
	
	public Cargo getCargo() {
		return cargo;
	}
	
	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}
	
	public Endereco getEndereco() {
		return endereco;
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	@Override
	public String toString() {
		return "Funcionario [id=" + id + ", nome=" + nome + ", dataEntrada=" + dataEntrada + ", dataSaida=" + dataSaida
				+ ", salario=" + salario + ", cargo=" + cargo + ", endereco=" + endereco + "]";
	}
	
}
