package br.com.devmedia.appfinal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.devmedia.appfinal.entity.Cargo;
import br.com.devmedia.appfinal.entity.Endereco;
import br.com.devmedia.appfinal.entity.Funcionario;

@Repository
public class FuncionarioDao extends GenericDao<Funcionario> {

	private CargoDao cargoDao;
	private EnderecoDao enderecoDao;
	
	@Autowired
	public FuncionarioDao(DataSource dataSource, CargoDao cargoDao, EnderecoDao enderecoDao) {
		super(dataSource, Funcionario.class);
		this.cargoDao = cargoDao;
		this.enderecoDao = enderecoDao;
	}

	@Override
	public SqlParameterSource parameterSource(Funcionario funcionario) {
		MapSqlParameterSource source = new MapSqlParameterSource();
		source.addValue("nome", funcionario.getNome());
		source.addValue("salario", funcionario.getSalario());
		source.addValue("dataEntrada", java.sql.Date.valueOf(funcionario.getDataEntrada()));
		source.addValue("id", funcionario.getId());
		source.addValue("cargo_id", funcionario.getCargo().getId());
		source.addValue("endereco_id", funcionario.getEndereco().getId());
		
		if (funcionario.getDataSaida() != null) {
			source.addValue("dataSaida", java.sql.Date.valueOf(funcionario.getDataSaida()));
		}
		
		return source;
	}

	@Override
	protected RowMapper<Funcionario> rowMapper() {
		return new RowMapper<Funcionario>() {

			public Funcionario mapRow(ResultSet rs, int rowNum) throws SQLException {
				Funcionario funcionario = new Funcionario();
				funcionario.setId(rs.getInt("id"));
				funcionario.setNome(rs.getString("nome"));
				funcionario.setDataEntrada(rs.getDate("data_entrada").toLocalDate());
				funcionario.setSalario(rs.getDouble("salario"));
				if (rs.getDate("data_saida") != null) {
					funcionario.setDataSaida(rs.getDate("data_saida").toLocalDate());
				}
				
				Endereco endereco = enderecoDao.findByID(rs.getInt("endereco_id"));
				funcionario.setEndereco(endereco);
				
				Cargo cargo = cargoDao.findByID(rs.getInt("cargo_id"));
				funcionario.setCargo(cargo);
				
				return funcionario;
			}
			
		};
	}
	
	public Funcionario save(Funcionario funcionario) {
		Number key = super.save("funcionarios", "id", parameterSource(funcionario));
		funcionario.setId(key.intValue());
		return funcionario;
	}
	
	public int update(Funcionario funcionario) {
		String dataSaida = "";
		if (funcionario.getDataSaida() != null) {
			dataSaida = "data_saida = :dataSaida, ";
		}
		String sql = "UPDATE funcionarios "
				+ "SET nome = :nome, salario = :salario, "
				+ "cargo_id = :cargo_id, endereco_id = :endereco_id, "
				+ dataSaida + "data_entrada = :dataEntrada "
				+ "WHERE id = :id";
		return super.update(sql, parameterSource(funcionario));
	}
	
	public int delete(Integer id) {
		String sql = "DELETE FROM funcionarios WHERE id = ?";
		return super.delete(sql, id);
	}
	
	public Funcionario findByID(Integer id) {
		String sql = "SELECT * FROM funcionarios WHERE id = ?";
		return super.findByID(sql, id, rowMapper());
	}
	
	public List<Funcionario> findAll() {
		String sql = "SELECT * FROM funcionarios";
		return super.findAll(sql, rowMapper());
	}
	
	public List<Funcionario> findByCargo(Integer cargoID) {
		String sql = "SELECT * FROM funcionarios "
				+ "WHERE cargo_id = :cargo_id";
		return namedQuery().query(
				sql,
				new MapSqlParameterSource("cargo_id", cargoID),
				rowMapper()
			);
	}
	
	public List<Funcionario> findByNome(String nome) {
		String sql = "SELECT * FROM funcionarios "
				+ "WHERE nome like :nome";
		return namedQuery().query(
				sql,
				new MapSqlParameterSource("nome","%" + nome + "%"),
				rowMapper()
			);
	}
	
	public List<Funcionario> findByPage(int page, int size) {
		String sql = "SELECT * FROM funcionarios "
				+ "LIMIT :page, :size";
		return namedQuery().query(
				sql,
				new MapSqlParameterSource("page", page).addValue("size", size),
				rowMapper()
			);
	}
	
}
