package br.com.devmedia.appfinal.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.devmedia.appfinal.entity.Cargo;
import br.com.devmedia.appfinal.entity.Departamento;

@Repository
public class CargoDao extends GenericDao<Cargo> {

	@Autowired
	private DepartamentoDao departamentoDao;
	
	@Autowired
	public CargoDao(DataSource dataSource) {
		super(dataSource, Cargo.class);
	}

	@Override
	public SqlParameterSource parameterSource(Cargo cargo) {
		return new MapSqlParameterSource()
			.addValue("cargo", cargo.getCargo())
			.addValue("departamento_id", cargo.getDepartamento().getId())
			.addValue("id", cargo.getId());
	}

	@Override
	protected RowMapper<Cargo> rowMapper() {
		return new RowMapper<Cargo>() {

			public Cargo mapRow(ResultSet rs, int rowNum) throws SQLException {
				Cargo cargo = new Cargo();
				cargo.setId(rs.getInt("id"));
				cargo.setCargo(rs.getString("cargo"));
				
				Integer id = rs.getInt("departamento_id");
				Departamento departamento = departamentoDao.findByID(id);
				cargo.setDepartamento(departamento);
				return cargo;
			}
			
		};
	}
	
	public Cargo save(Cargo cargo) {
		Number key = super.save("cargos", "id", parameterSource(cargo));
		cargo.setId(key.intValue());
		return cargo;
	}
	
	public int update(Cargo cargo) {
		String sql = "UPDATE cargos "
				+ "SET cargo = :cargo, departamento_id = :departamento_id "
				+ "WHERE id = :id";
		return super.update(sql, parameterSource(cargo));
	}
	
	public int delete(Integer id) {
		String sql = "DELETE FROM cargos "
				+ "WHERE id = ?";
		return super.delete(sql, id);
	}
	
	public Cargo findByID(Integer id) {
		String sql = "SELECT * FROM cargos "
				+ "WHERE id = ?";
		return super.findByID(sql, id, rowMapper());
	}
	
	public List<Cargo> findAll() {
		String sql = "SELECT * FROM cargos";
		return super.findAll(sql, rowMapper());
	}
	
	public List<Cargo> findByPage(int page, int size) {
		return namedQuery().query(
				"SELECT * FROM cargos LIMIT :page, :size",
				new MapSqlParameterSource("page", page).addValue("size", size),
				rowMapper());
	}
	
}
