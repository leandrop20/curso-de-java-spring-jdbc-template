package br.com.devmedia.appfinal.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.devmedia.appfinal.entity.Departamento;

@Repository
public class DepartamentoDao extends GenericDao<Departamento> {
	
	@Autowired
	public DepartamentoDao(DataSource dataSource) {
		super(dataSource, Departamento.class);
	}
	
	@Override
	public SqlParameterSource parameterSource(Departamento departamento) {
		return new BeanPropertySqlParameterSource(departamento);
	}
	
	@Override
	protected RowMapper<Departamento> rowMapper() {
		return new BeanPropertyRowMapper<Departamento>(Departamento.class);
	}
	
	public Departamento save(Departamento departamento) {
		Number key = super.save("departamentos", "id", parameterSource(departamento));
		departamento.setId(key.intValue());
		return departamento;
	}
	
	public int update(Departamento departamento) {
		String sql = "UPDATE departamentos SET departamento = "
				+ ":departamento WHERE id = :id";
		return super.update(sql, parameterSource(departamento));
	}
	
	public int delete(Integer id) {
		String sql = "DELETE FROM departamentos"
				+ " WHERE id = ?";
		return super.delete(sql, id);
	}
	
	public Departamento findByID(Integer id) {
		String sql = "SELECT * FROM departamentos"
				+ " WHERE id = ?";
		return super.findByID(sql, id, rowMapper());
	}
	
	public List<Departamento> findAll() {
		String sql = "SELECT * FROM departamentos";
		return super.findAll(sql, rowMapper());
	}
	
	public List<Departamento> findByPage(int page, int size) {
		String sql = "SELECT * FROM departamentos "
				+ "LIMIT :page, :size";
		return namedQuery().query(
				sql,
				new MapSqlParameterSource("page", page).addValue("size", size),
				rowMapper());
	}
	
}