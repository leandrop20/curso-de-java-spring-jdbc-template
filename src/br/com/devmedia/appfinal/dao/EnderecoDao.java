package br.com.devmedia.appfinal.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import br.com.devmedia.appfinal.entity.Endereco;

@Repository
public class EnderecoDao extends GenericDao<Endereco> {

	@Autowired
	public EnderecoDao(DataSource dataSource) {
		super(dataSource, Endereco.class);
	}
	
	@Override
	public SqlParameterSource parameterSource(Endereco endero) {
		return new BeanPropertySqlParameterSource(endero);
	}

	@Override
	protected RowMapper<Endereco> rowMapper() {
		return new BeanPropertyRowMapper<Endereco>(Endereco.class);
	}
	
	public Endereco save(Endereco endereco) {
		Number key = super.save("enderecos", "id", parameterSource(endereco));
		endereco.setId(key.intValue());
		return endereco;
	}
	
	public int update(Endereco endereco) {
		String sql = "UPDATE enderecos "
				+ "SET logradouro = :logradouro, numero = :numero, "
				+ "complemento = :complemento, estado = :estado, "
				+ "bairro = :bairro, cidade = :cidade "
				+ "WHERE id = :id";
		return super.update(sql, parameterSource(endereco));
	}
	
	public int delete(Integer id) {
		String sql = "DELETE FROM enderecos WHERE id = ?";
		return super.delete(sql, id);
	}
	
	public Endereco findByID(Integer id) {
		String sql = "SELECT * FROM enderecos WHERE id = ?";
		return super.findByID(sql, id, rowMapper());
	}
	
	public List<Endereco> findAll() {
		String sql = "SELECT * FROM enderecos";
		return super.findAll(sql, rowMapper());
	}
	
}
